import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Diagnostic } from "@ionic-native/diagnostic";
import { Http, Headers, RequestOptions } from '@angular/http';
import { LocationTrackerProvider } from "../../providers/location-tracker/location-tracker";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    public myLoginObject: any;
    // public baseUrl: string = '/ProxyBase/';
    public baseUrl: string = 'https://sangam.themedemo.tk/api';

    constructor(
        public navCtrl: NavController,
        public diagnostic: Diagnostic,
        public platform: Platform,
        public locationTracker: LocationTrackerProvider,
        public http: Http
    ) {
    }


    public sendGetRequest(): void {
        this.http.get(`${this.baseUrl}/login?username=admin@shreewebs.com&password=admin`).map(res => res.json()).subscribe(data => {
            console.log('Data : ', JSON.stringify(data));
            this.myLoginObject = JSON.stringify(data);
        });
    };

    public start() {
        this.platform.ready().then(() => {
            this.diagnostic.isLocationEnabled().then((available) => {
                if (available) {
                    this.locationTracker.startTracking();
                } else {
                    this.diagnostic.switchToLocationSettings();
                }
            });
        });
    }

    public stop() {
        this.locationTracker.stopTracking();
    }

}
