import { Injectable, NgZone } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

declare var BackgroundGeolocation: any;

@Injectable()
export class LocationTrackerProvider {
    public watch: any;
    public lat: number = 0;
    public lng: number = 0;
    public currentDateTime: any;
    public locations: location[] = [];
    public locations2: any = [];

    constructor(
        public zone: NgZone,
        public geolocation: Geolocation,
    ) {
    }

    public startTracking() {
        BackgroundGeolocation.configure({
            locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
            desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
            stationaryRadius: 50,
            distanceFilter: 50,
            notificationTitle: 'Background tracking',
            notificationText: 'enabled',
            debug: false,
            interval: 100,
            fastestInterval: 500,
            activitiesInterval: 100,
            url: 'https://sangam.themedemo.tk/api/add_user',
            httpHeaders: {},
            // customize post properties
            postTemplate: {
                lat: this.lat,
                lon: this.lng,
                userid: 3,
                projectid: 7
            }
        });

        BackgroundGeolocation.on('location', function (location) {
            // handle your locations here
            console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);
            // Update inside of Angular's zone
            this.zone.run(() => {
                this.lat = location.latitude;
                this.lng = location.longitude;
                let newLocation: location = {
                    lat: location.latitude,
                    lng: location.longitude
                };
                this.locations.push(newLocation);
            });
            // to perform long running operation on iOS
            // you need to create background task
            BackgroundGeolocation.startTask(function (taskKey) {
                // execute long running task
                // eg. ajax post location
                // IMPORTANT: task has to be ended by endTask
                BackgroundGeolocation.endTask(taskKey);
            });
        });

        BackgroundGeolocation.on('stationary', function (stationaryLocation) {
            // handle stationary locations here
            console.log('stationaryLocation BackgroundGeolocation:  ' + stationaryLocation.latitude + ',' + stationaryLocation.longitude);
        });

        BackgroundGeolocation.on('error', function (error) {
            console.log('[ERROR] BackgroundGeolocation error:', error.code, error.message);
        });

        BackgroundGeolocation.on('background', function () {
            console.log('[INFO] App is in background');
            // you can also reconfigure service (changes will be applied immediately)
            BackgroundGeolocation.configure({ debug: false });
        });

        BackgroundGeolocation.on('foreground', function () {
            console.log('[INFO] App is in foreground');
            BackgroundGeolocation.configure({ debug: false });
        });

        BackgroundGeolocation.on('start', function () {
            console.log('[INFO] BackgroundGeolocation service has been started');
        });

        BackgroundGeolocation.on('stop', function () {
            console.log('[INFO] BackgroundGeolocation service has been stopped');
        });

        BackgroundGeolocation.checkStatus(function (status) {
            console.log('[INFO] BackgroundGeolocation service is running', status.isRunning);
            console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled);
            console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization);

            // you don't need to check status before start (this is just the example)
            if (!status.isRunning) {
                BackgroundGeolocation.start(); //triggers start on start event
            }
        });

        // Background tracking
        let options = {
            frequency: 3000,
            enableHighAccuracy: true
        };

        this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {
            console.log(position);
            let newLocation: any = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                currentDateTime: new Date()
            };
            this.locations2.push(newLocation);
            this.zone.run(() => {
                this.lat = position.coords.latitude;
                this.lng = position.coords.longitude;
            });
        });
    }

    public stopTracking() {
        /* console.log('stopTracking');
        alert("STOP TRACKING");
        alert(this.locations.length);
        alert(this.locations);
        alert("STOP TRACKING2");
        alert(this.locations2.length);
        alert(this.locations2); */
        BackgroundGeolocation.stop(); //triggers start on start event
        this.watch.unsubscribe();

    }
}
