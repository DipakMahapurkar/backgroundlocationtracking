import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import { Diagnostic } from '@ionic-native/diagnostic';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {Geolocation} from "@ionic-native/geolocation";
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Diagnostic,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    LocationTrackerProvider
  ]
})
export class AppModule {
}
